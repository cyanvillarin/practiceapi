// index.js

'use strict'

const express = require('express')
const bodyparser = require('body-parser')
const app = express()
const pg = require('pg')

const local_config = {
    host: 'localhost',
    user: 'cyanvillarin',
    database: 'node_hero',
    password: 'bnbn',
    port: 5432
};

const remote_config = {
    host: 'ec2-50-19-254-63.compute-1.amazonaws.com',
    user: 'zfqfrvsoiudjle',
    database: 'd30u04iih4tv3h',
    password: '4b5773f3b310f14f35088631c31ed02f2f9a313c0e8040bd529901248bccba15',
    port: 5432
};

const pool = new pg.Pool(remote_config);

app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended:true}))

app.post('/users', function (req, res, next) {
    pool.connect(function (err, client, done) {
        if (err) {
            return next(err)
        }
        const { name, age } = req.body
        client.query("INSERT INTO users (name, age) VALUES ($1, $2);", [name, age], function (err, result) {
            done()

            if (err) {
                return next(err)
            }
            res.sendStatus(200)
        })
    })
})

app.get('/users', function (req, res, next) {
    pool.connect(function (err, client, done) {
        if (err) {
            return next(err)
        }
        client.query('SELECT name, age FROM users;', [], function (err, result) {
            done()

            if (err) {
                return next(err)
            }
            res.json(result.rows)
        })
    })
})

// app.listen(3000)
app.listen(process.env.PORT || 3000)